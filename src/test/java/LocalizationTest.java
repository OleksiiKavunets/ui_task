import common.BaseTest;
import core.common.env.Environment;
import core.common.pojo.User;
import core.pageobject.pages.LoginPage;
import core.pageobject.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class LocalizationTest extends BaseTest {

    @Test
    public void testCanSaveUserLanguage(){

        Environment env = Environment.getInstance();
        User user = new User(env.getEmail(), env.getPassword());
        LoginPage loginPage = new LoginPage(driver);

        String actualLanguage = loginPage.open()
                .switchLanguage("en")
                .loginAs(user)
        .footerPanel.getActiveLanguage();

        Assert.assertEquals(actualLanguage, "Ru", "User language was changed");
    }

    @AfterMethod
    public void signOut(){
        new MainPage(driver).headerPanel.signOut();
    }

}
