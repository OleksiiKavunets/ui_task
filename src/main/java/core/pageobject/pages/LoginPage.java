package core.pageobject.pages;

import core.common.env.Environment;
import core.common.pojo.User;
import core.pageobject.pages.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.stream.IntStream;

public class LoginPage extends AbstractPage {

    @FindBy(name = "email")
    WebElement emailInput;

    @FindBy(name = "password")
    WebElement passwordInput;

    @FindBy(css = "a[class=\"home-page-form__submit js_submit\"]")
    WebElement submitBtn;

    @FindBy(css = "[class*=\"languages__icon\"]")
    WebElement currentLang;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open(){
        String url = Environment.getInstance().getBaseUrl();
        try{
            getDriver().get(url);
        }catch (TimeoutException e1){
            IntStream.range(0, 10).forEach(s -> {
                try {
                    getDriver().get(url);
                }catch (TimeoutException e2){}

        });
        }

        return this;
    }

    public MainPage loginAs(User user){
        emailInput.sendKeys(user.getEmail());
        passwordInput.sendKeys(user.getPassword());
        submitBtn.click();
        return new MainPage(getDriver());
    }

    public LoginPage switchLanguage(String language){
        getWait().until(ExpectedConditions.visibilityOf(currentLang)).click();
        getDriver().findElement(new By.ByPartialLinkText(language)).click();
        waitForLanguageToBeSwitched(language);
        return this;
    }

    public boolean waitForPageToOpen(){
        return getWait().until(ExpectedConditions.visibilityOf(emailInput)).isDisplayed();
    }

    private LoginPage waitForLanguageToBeSwitched(String language){
        getWait().until(ExpectedConditions.attributeContains(currentLang, "class", language));
        return this;
    }

}
