package core.pageobject.pages;

import core.pageobject.pages.common.AbstractPage;
import core.pageobject.panels.FooterPanel;
import core.pageobject.panels.HeaderPanel;
import org.openqa.selenium.WebDriver;

public class MainPage extends AbstractPage {

    public HeaderPanel headerPanel = new HeaderPanel(getDriver(), "[class=\"new-header\"]");
    public FooterPanel footerPanel = new FooterPanel(getDriver(), "div[class=\"new-footer\"]");

    public MainPage(WebDriver driver) {
        super(driver);
    }
}
