package core.pageobject.panels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FooterPanel {

    private WebElement panel;
    private WebDriverWait wait;

    private String activeLanguage = "a[class=\"new-footer-lang__link active\"]";

    public FooterPanel(WebDriver driver, String locator){
        wait = new WebDriverWait(driver, 60);
        panel = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
    }

    public String getActiveLanguage(){
        return panel.findElement(By.cssSelector(activeLanguage)).getText();
    }
}
