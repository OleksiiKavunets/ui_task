package core.pageobject.panels;

import core.pageobject.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HeaderPanel {

    private String dropdownMenu = "li[class*=\"new-header-sub-menu\"]";
    private String signOutBtn = "[href=\"/sign/out\"]";

    private WebElement panel;
    private WebDriver driver;

    public HeaderPanel(WebDriver driver, String locator){
        this.driver = driver;
        panel = driver.findElement(By.cssSelector(locator));
    }

    public LoginPage signOut(){
        LoginPage loginPage = new LoginPage(driver);
        if(panel.isDisplayed()) {
            panel.findElement(By.cssSelector(dropdownMenu)).click();
            panel.findElement(By.cssSelector(signOutBtn)).click();
            loginPage.waitForPageToOpen();
        }
        return loginPage;
    }
}
