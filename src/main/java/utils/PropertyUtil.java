package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {

    private String PARTIAL_PROPERTIES_PATH = "src/main/resources/properties";
    private String PATH;
    private Properties properties = new Properties();

    public PropertyUtil(String path){
        PATH = new StringBuilder(PARTIAL_PROPERTIES_PATH).append(path).toString();
        readProperties(PATH);
    }

    private void readProperties(String path){

        try (final InputStream inputStream = new FileInputStream(new File(path))) {
            properties.load(inputStream);
        } catch (final IOException e) {
            throw new IllegalStateException("Something has come up while reading properties!", e);
        }
    }

    public String getProperty(final String propertyKey){
        return properties.getProperty(propertyKey);
    }
}

